# libgo - script to build library.gnome.org
# Copyright (C) 2007-2009  Frederic Peters
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301  USA

import logging
import os
import re
import time
import urllib2
import sys
import tarfile
import lzma # pyliblzma


class FakeTarFile:
    '''
    The FakeTarFile provides a TarFile-like interface over an already extracted
    directory.  There is just enough methods for libgo to work, it is not a
    complete or perfect replacemen.
    '''
    def __init__(self, path):
        self.path = path
        self.name = os.path.basename(path) + '.tar'
        self._loaded = False
        self.load()

    def load(self):
        self.members = []
        for base, dir, files in os.walk(self.path):
            self.members.append(FakeTarInfo(self.path, base + '/'))
            for f in files:
                self.members.append(FakeTarInfo(self.path, os.path.join(base, f)))

    def __iter__(self):
        return iter(self.members)

    def getmembers(self):
        return self.members

    def close(self):
        pass
    
    def extractfile(self, tarinfo):
        return open(tarinfo.path)


class FakeTarInfo:
    def __init__(self, base, path):
        self.base = base
        self.path = path
        self.name = path[len(self.base) - len(os.path.split(self.base)[-1]):]

    def __repr__(self):
        return '<FakeTarInfo \'%s\'>' % self.name

    def isdir(self):
        return os.path.isdir(self.path)

def tryint(x):
    try:
        return int(x)
    except ValueError:
        return x

def version_cmp(x, y):
    # returns < 0 if x < y, 0 if x == y, and > 0 if x > y
    if x == 'nightly' and y == 'nightly':
        return 0
    elif x == 'nightly':
        return 1
    elif y == 'nightly':
        return -1
    try:
        return cmp([tryint(j) for j in x.split('.')], [tryint(k) for k in y.split('.')])
    except ValueError:
        logging.warning('failure in version_cmp: %r vs %r' % (x, y))
        return 0

def is_version_number(v):
    return re.match('\d+\.\d+', v) is not None

class LogFormatter(logging.Formatter):
    '''Class used for formatting log messages'''

    def __init__(self):
        term = os.environ.get('TERM', '')
        self.is_screen = (term == 'screen')
        logging.Formatter.__init__(self)

    def format(self, record):
        if self.is_screen and record.levelname[0] == 'I':
            sys.stdout.write('\033klgo: %s\033\\' % record.msg)
            sys.stdout.flush()
        return '%c:[%s] %s' % (record.levelname[0], time.strftime('%Y%m%d %H:%M:%S'), record.msg)


class _LZMAProxy(object):
    """Small proxy class that enables external file object
       support for "r:lzma" and "w:lzma" modes. This is actually
       a workaround for a limitation in lzma module's LZMAFile
       class which (unlike gzip.GzipFile) has no support for
       a file object argument.
    """

    blocksize = 16 * 1024

    def __init__(self, fileobj, mode):
        self.fileobj = fileobj
        self.mode = mode
        self.name = getattr(self.fileobj, "name", None)
        self.init()

    def init(self):
        self.pos = 0
        if self.mode == "r":
            self.lzmaobj = lzma.LZMADecompressor()
            self.fileobj.seek(0)
            self.buf = ""
        else:
            self.lzmaobj = lzma.LZMACompressor()

    def read(self, size):
        b = [self.buf]
        x = len(self.buf)
        while x < size:
            raw = self.fileobj.read(self.blocksize)
            if not raw:
                break
            try:
                data = self.lzmaobj.decompress(raw)
            except EOFError:
                break
            b.append(data)
            x += len(data)
        self.buf = "".join(b)

        buf = self.buf[:size]
        self.buf = self.buf[size:]
        self.pos += len(buf)
        return buf

    def seek(self, pos):
        if pos < self.pos:
            self.init()
        self.read(pos - self.pos)


class XzTarFile(tarfile.TarFile):

    OPEN_METH = tarfile.TarFile.OPEN_METH.copy()
    OPEN_METH["xz"] = "xzopen"

    @classmethod
    def xzopen(cls, name, mode="r", fileobj=None, **kwargs):
        """Open gzip compressed tar archive name for reading or writing.
           Appending is not allowed.
        """
        if len(mode) > 1 or mode not in "rw":
            raise ValueError("mode must be 'r' or 'w'")

        if fileobj is not None:
            fileobj = _LMZAProxy(fileobj, mode)
        else:
            fileobj = lzma.LZMAFile(name, mode)

        try:
            # lzma doesn't immediately return an error
            # try and read a bit of data to determine if it is a valid xz file
            fileobj.read(_LZMAProxy.blocksize)
            fileobj.seek(0)
            t = cls.taropen(name, mode, fileobj, **kwargs)
        except IOError:
            raise tarfile.ReadError("not a xz file")
        except lzma.error:
            raise tarfile.ReadError("not a xz file")
        t._extfileobj = False
        return t

if not hasattr(tarfile.TarFile, 'xvopen'):
    tarfile.open = XzTarFile.open

