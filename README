==========================================
libgo -- script to build library.gnome.org
==========================================


Installation
============

libgo is made to run from its checkout directory, there is no need for
installation.

configure and make are used to build translations, running ./autogen.sh
&& make will build the necessary bits.  Dependencies are gettext,
xsltproc, gnome-doc-utils, and yelp-tools.


Usage
=====

usage: lgo.py [options]

options:
  -h, --help            show this help message and exit
  -c CONFIG, --config=CONFIG
  -v, --verbose         verbosity level (more -v for more verbose)
  --rebuild=FILENAME    rebuild documentation from FILENAME
  --rebuild-all         rebuild all documents (even those that were already
                        built)
  --rebuild-language=LANGUAGE
                        rebuild all documents in LANGUAGE
  --skip-extra-tarballs
                        don't look for documentation extra tarballs

Example: python src/lgo.py -c sample.lgorc -v -v -v

Note about verbosity: xsltproc output is not redirected to /dev/null and will
amount for most of the output.


Configuration File
==================

lgo will look at ~/.lgorc, it can be overridden with -c.

sample.lgorc is a sample configuration file, src/defaults.lgorc lists all
variables and their default settings.

Notable configuration variables are:

 - ftp_gnome_org_local_copy: path to local copy of ftp.gnome.org, this is
   used not to download all the files that are already local.

 - download_cache_dir: path to download cache directory (default:
   ~/.lgo/cache/)

 - output_dir: path to web output (default: /var/www/library.gnome.org/)

 - version_min, version_max: minimum/maximum GNOME version to have
   documentation for (absolute minimum is 2.12.0).

   Note about versions:
    - libgo will only build the latest version of a serie,
    - libgo will only build the latest version of the latest development
      serie.

 - modules: list of modules to use (default: None, meaning all modules);
   this can be used to restrict modules to a specific subset, example:
   ['eog', 'gedit']

 - languages: list of languages to build (default: None, meaning all
   languages); this can be used to restrict languages to a specific subset,
   example: ['en', 'sv', 'ja', 'es']


Apache Configuration
====================

libgo generates a static website, to be served by Apache using its MultiViews
mode for language negociation, a working virtualhost would be:

  <VirtualHost *:80>
    ServerName library.gnome.org
    DocumentRoot /var/www/library.gnome.org/

    <Directory /var/www/library.gnome.org/>
      Options All
      AllowOverride All
      Require all granted
      Options +MultiViews
    </Directory>

    # Make it possible to use a cookie to negociate language
    LanguagePriority en
    ForceLanguagePriority Prefer Fallback
    SetEnvIf Cookie "language=(.+)" prefer-language=$1 Header append
    Vary cookie
    Header append Vary Cookie
  </VirtualHost>


System Requirements
===================

To build everything from 2.12 to current 2.19 development version :

 - disk usage:
   - download cache (everything from ftp.gnome.org): 829M
   - extracted files: 679M
   - web site: 624M

 - time:
   - building and scanning all documentation: 2h40
   - scanning (already built) documentation: 0h20


Mode of operation
=================

- Using http://ftp.gnome.org/pub/GNOME/teams/releng/ to download GNOME jhbuild
  modulesets for requested versions
- Iterating on versions
  - Downloading tarballs
    - Looking for gtk-doc or gnome-doc-utils usage in tarball
      - Building them
- Creating indexes

