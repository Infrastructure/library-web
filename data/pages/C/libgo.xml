<?xml version="1.0"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
"http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
]>

<article id="index" lang="en">
  <articleinfo>
    <title>GNOME Library Help</title>

    <publisher role="maintainer">
      <publishername> GNOME Documentation Project </publishername>
    </publisher>
    <authorgroup>
      <author>
        <firstname>Goran</firstname>
        <surname>Rakic</surname>
      </author>
      <author>
        <firstname>Frederic</firstname>
        <surname>Peters</surname>
      </author>
    </authorgroup>

    <revhistory>
      <revision>
        <revnumber>GNOME Library Help v1</revnumber>
        <date>May 2008</date>
      </revision>
      <revision>
        <revnumber>GNOME Library Help v0</revnumber>
        <date>July 2007</date>
      </revision>
    </revhistory>

    <abstract role="description">
      <para>Information pages on library.gnome.org</para>
    </abstract>
  </articleinfo>

  <section id="about">
    <title>About library.gnome.org</title>
    <para>
      library.gnome.org aims to be the central place for documentation related
      to the GNOME project, be it for users, system administrators or
      developers.
    </para>

    <para>
      Its development tree is hosted in the GNOME GIT repository, in the library-web
      module.
    </para>
  </section>

  <section id="langinfo">
    <title>This web site in different languages</title>
    <para>
      When you request a document from a server, your language preference is
      passed via HTTP Accept-Language header. This is a standard way to deliver
      the preferred version of a page available in more than one language. It is
      very important to setup your browser language preference correctly.
    </para>

    <section>
      <title>What to do if page is in wrong language</title>

      <para>
        The usual reason is that your web browser is improperly configured.
        Skip to <ulink url="#setup-language-settings">How to set up the language
        settings section</ulink> for more informations. You may use this
        <ulink url="http://www.cs.tut.fi/cgi-bin/run/~jkorpela/lang.cgi">online
        tool</ulink> to see what languages your browser requests.
      </para>

      <para>
        If your browser is configured, the problem may be that your ISP or your
        internal network is using misconfigured cache or proxy server. They are
        servers without any content that sits in the middle between users and
        real web servers. They grab your requests for web pages and fetch the
        page. After that, they forward the page to you but also make a local,
        cached copy, for later requests. This can really cut down on network
        traffic when many users request the same page.
      </para>

      <para>
        Problem is when they do not understand content negotiation and keep
        cached copy of a page in only one language. The only way you can fix
        this is to complain to your ISP in order for them to upgrade or replace
        their software.
      </para>

      <para>
        Third problem may be that you are unable or do not want to change
        browser settings. If you have JavaScript and cookies enabled, our
        server will store your language preference in a cookie every time you
        change language manually by clicking on a link. The next time you click
        on link to another page, your language selection will be preserved.
      </para>
    </section>

    <section>
      <title id="setup-language-settings">How to set up the language settings</title>

      <para>
        Generally, you should include all languages you can read in preferred
        language, ordered by preference. It is good idea to include English
        ('en') as last item in your list, to use it as a fallback language. You
        need to be careful with sub-categories of languages, en-GB will not
        include en, so you need to include both en and en-GB in your list.
      </para>

      <variablelist>
        <varlistentry>
          <term>Epiphany</term>
          <listitem><para>
            <menuchoice>
              <guimenu>Edit</guimenu>
              <guimenuitem>Preferences</guimenuitem>
              <guimenuitem>Language</guimenuitem>
            </menuchoice>
          </para></listitem>
        </varlistentry>

        <varlistentry>
          <term>Firefox / Iceweasel</term>
          <listitem><para>
            <menuchoice>
              <guimenu>Edit</guimenu>
              <guimenuitem>Preferences</guimenuitem>
              <guimenuitem>Content</guimenuitem>
              <guimenuitem>Languages</guimenuitem>
            </menuchoice>
          </para></listitem>
        </varlistentry>

        <varlistentry>
          <term>Galeon</term>
          <listitem><para>
            <menuchoice>
              <guimenu>Edit</guimenu>
              <guimenuitem>Preferences</guimenuitem>
              <guimenuitem>Languages</guimenuitem>
            </menuchoice>
          </para></listitem>
        </varlistentry>

        <varlistentry>
          <term>Mozilla / Netscape Navigator</term>
          <listitem><para>
            <menuchoice>
              <guimenu>Edit</guimenu>
              <guimenuitem>Preferences</guimenuitem>
              <guimenuitem>Navigator</guimenuitem>
              <guimenuitem>Languages</guimenuitem>
            </menuchoice>
          </para></listitem>
        </varlistentry>

        <varlistentry>
          <term>Lynx</term>
          <listitem><para>
            <menuchoice>
              <guimenu>O (Options)</guimenu>
              <guimenuitem>Headers Transferred to Remote Servers</guimenuitem>
              <guimenuitem>Preferred document language</guimenuitem>
            </menuchoice>
          </para></listitem>
        </varlistentry>

        <varlistentry>
          <term>Amaya</term>
          <listitem><para>
            <menuchoice>
              <guimenu>Edit</guimenu>
              <guimenuitem>Preferences</guimenuitem>
              <guimenuitem>Browsing</guimenuitem>
              <guimenuitem>List of preferred languages</guimenuitem>
            </menuchoice>
          </para></listitem>
        </varlistentry>

        <varlistentry>
          <term>Internet Explorer</term>
          <listitem><para>
            <menuchoice>
              <guimenu>Tools</guimenu>
              <guimenuitem>Internet Options</guimenuitem>
              <guimenuitem>General</guimenuitem>
              <guimenuitem>Languages</guimenuitem>
            </menuchoice>
          </para></listitem>
        </varlistentry>

        <varlistentry>
          <term>Opera</term>
          <listitem><para>
            <menuchoice>
              <guimenu>File</guimenu>
              <guimenuitem>Preferences</guimenuitem>
              <guimenuitem>Languages</guimenuitem>
            </menuchoice>
          </para></listitem>
        </varlistentry>

        <varlistentry>
          <term>Safari</term>
          <listitem><para>
            <menuchoice>
              <guimenu>Mac OS X System preferences</guimenu>
              <guimenuitem>International</guimenuitem>
              <guimenuitem>Language</guimenuitem>
            </menuchoice>
          </para></listitem>
        </varlistentry>



      </variablelist>

    </section>

    <section>
      <title>About translations</title>

      <para>
        All translations are available as an effort of <ulink
        url="https://wiki.gnome.org/TranslationProject">Gnome Translation
        Project (GTP)</ulink>.
      </para>

    </section>

  </section>

  <section id="feedback">
    <title>Feedback</title>

    <para> This section contains information on reporting bugs about this
    website and requesting for new documents to be added.  </para>

    <para> If you have found a bug on this website, please report it!
    Developers do read all the bug reports and try to fix these bugs. Please
    try to be as specific as possible when describing the circumstances under
    which the bug shows (what was the URL?, which browser are you using?, do
    you have cookies enabled?).  If there were any error messages, be sure to
    include them, too.  </para> 

    <para> You can submit bugs and browse the list of known bugs by connecting
    to the <ulink type="http" url="https://gitlab.gnome.org/">GNOME bug
    tracking database</ulink>.  You will need to register before you can submit
    any bugs this way — and do not forget to read <ulink type="http"
    url="https://wiki.gnome.org/Community/GettingInTouch/BugReportingGuidelines">Bug
    Writing Guidelines</ulink>.  </para>

    <para> GNOME Library bugs are filed under <ulink type="http"
    url="https://gitlab.gnome.org/Infrastructure/library-web/issues/">Infrastructure/library-web</ulink>.</para>

    <section>
      <title>Requesting for a documentation to be added</title>

      <para>Library.gnome.org has support for both gnome-doc-utils (manuals)
      and gtk-doc (references) as well as some minimalistic support for
      straight passing of HTML files through XSL stylesheets.  It can also
      display links to resource available on other websites.</para>

      <para>Modules that are part of the GNOME JHBuild modulesets, as published
      by the release team, are automatically added to the library.  For other
      modules you have to file a GNOME Library bug under <ulink type="http"
      url="https://gitlab.gnome.org/Infrastructure/library-web/issues/">Infrastructure/library-web</ulink>.</para>

      <para>Please note Library.gnome.org only operates on tarballs, there is
      no support for documents that are only available through a revision
      control system, so the first thing is to make sure when requesting for a
      new document to be added is that a tarball is available.  It also has
      some special support for tarballs hosted on download.gnome.org and will
      automatically pick up new versions.</para>

    </section>

  </section>

</article>
