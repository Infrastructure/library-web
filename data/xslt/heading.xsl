<?xml version='1.0' encoding='UTF-8'?><!-- -*- indent-tabs-mode: nil -*- -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">

<xsl:import href="gettext.xsl"/>

<xsl:param name="libgo.channel">undefined</xsl:param>
<xsl:param name="libgo.dbm_support" select="false()"/>

<xsl:template name="libgo.header">
  <xsl:param name="channel"><xsl:value-of select="$libgo.channel"/></xsl:param>
  <xsl:param name="lang"><xsl:value-of select="$libgo.lang"/></xsl:param>


    <!-- accessibility access -->
    <div id="accessibility_access">
        <ul>
            <li><a href="#container">Go to page content</a></li>
            <li><a href="#top_bar">Go to main menu</a></li>
            <li><a href="#s" onclick="$('#s').focus(); return false;">Go to the search field</a></li>
        </ul>
    </div>
    
    <!-- global gnome.org domain bar -->
    <div id="global_domain_bar">
      <div class="maxwidth">
        <div class="tab">
          <a class="root" href="https://www.gnome.org/">GNOME.org</a>
        </div>
      </div>
    </div>
    
    
    <!-- header -->
    <div id="header" class="container_12">
        <div id="logo" class="grid_3">
            <a title="Go to home page" href="/"><img alt="GNOME: The Free Software Desktop Project">
              <xsl:attribute name="src">
                <xsl:choose>
                  <xsl:when test="$libgo.channel = 'devel'">/skin/gnome-logo-devcenter.png</xsl:when>
                  <xsl:otherwise>/skin/gnome-logo.png</xsl:otherwise>
                </xsl:choose>
              </xsl:attribute></img></a>
        </div>
        <div id="top_bar" class="grid_9">
            <div class="left">
                <div class="menu-globalnav-container"><ul id="menu-globalnav" class="menu">
                  <li class="menu-item menu-item-type-post_type menu-item-6">
                  <xsl:if test="$channel = 'about'">
                    <xsl:attribute name="class">current-menu-item</xsl:attribute>
                  </xsl:if>
                  <a href="/about/"><xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="$lang"/><xsl:with-param name="msgid"
                    select="'aboutlabel'"/></xsl:call-template>
                  </a>
                  </li>
                  <li class="menu-item menu-item-type-post_type menu-item-23">
                  <xsl:if test="$channel = 'users'">
                    <xsl:attribute name="class">current-menu-item</xsl:attribute>
                  </xsl:if>
                  <a href="https://help.gnome.org/users/"><xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="$lang"/><xsl:with-param name="msgid"
                    select="'userslabel'"/></xsl:call-template>
                  </a>
                  </li>

                  <li class="menu-item menu-item-type-post_type menu-item-40">
                  <xsl:if test="$channel = 'admin'">
                    <xsl:attribute name="class">current-menu-item</xsl:attribute>
                  </xsl:if>
                  <a href="https://help.gnome.org/admin/"><xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="$lang"/><xsl:with-param name="msgid"
                    select="'sysadminslabel'"/></xsl:call-template>
                  </a>
                  </li>

                  <li class="menu-item menu-item-type-post_type menu-item-37">
                  <xsl:if test="$channel = 'devel'">
                    <xsl:attribute name="class">current-menu-item</xsl:attribute>
                  </xsl:if>
                  <a href="https://developer.gnome.org/"><xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="$lang"/><xsl:with-param name="msgid"
                    select="'developerslabel'"/></xsl:call-template>
                  </a>
                  </li>
</ul></div>                            </div>
            <div class="right">
                <form role="search" method="get" id="searchform">
                  <xsl:attribute name="action">
                    <xsl:if test="$channel = 'devel' and $libgo.dbm_support">/symbols/</xsl:if>
                    <xsl:if test="$channel != 'devel' or not($libgo.dbm_support)">/search</xsl:if>
                  </xsl:attribute>
                    <div>
                        <label class="hidden" for="q">
                        <xsl:call-template name="gettext"><xsl:with-param name="lang"
                        select="$lang"/><xsl:with-param name="msgid"
                        select="'searchlabel'"/></xsl:call-template>:
                        </label><input type="text" value="" name="q" id="s" placeholder="Search" />
                    </div>
                </form>
                <xsl:if test="$channel = 'devel' and $libgo.dbm_support">
              <script type="text/javascript">
$('#s').autocomplete('/symbols/lookup/',
        { minChars:3, matchSubset:1, matchContains:1, cacheLength:10,
          selectOnly:1, rowsLimit:25 });
              </script>
                </xsl:if>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</xsl:template>

<xsl:template name="libgo.head">
  <xsl:param name="channel"><xsl:value-of select="$libgo.channel"/></xsl:param>
  <link rel="stylesheet" type="text/css" media="all"
        href="https://static.gnome.org/css/grass-theme-style.css" />
  <link rel="stylesheet" type="text/css" media="all" href="/skin/lgo2010.css"/>
  <!-- XXX <link media="print" rel="stylesheet" type="text/css" href="/skin/print.css"/> -->
  <link rel="icon" type="image/png" href="/skin/gnome-16.png"/>
  <link rel="SHORTCUT ICON" type="image/png" href="/skin/gnome-16.png"/>
  <link rel="search" type="application/opensearchdescription+xml"
    href="/gnome-library-search.xml" title="GNOME Library Search" />
  <xsl:if test="$libgo.dbm_support and $channel = 'devel'">
    <link rel="stylesheet" type="text/css" href="/skin/jquery.autocomplete.css"/>
    <script type="text/javascript" src="/js/jquery.js" />
    <script type="text/javascript" src="/js/jquery.autocomplete.js" />
  </xsl:if>
</xsl:template>

<xsl:template name="libgo.footer">
    <div class="clearfix"></div>
    <div id="footer_grass">   </div>
    <div id="footer">
        <div class="container_12">
            <div class="links grid_9">
                <div class="menu-footer-1-container"><ul id="menu-footer-1" class="menu"><li id="menu-item-88" class="menu-item menu-item-type-custom menu-item-home menu-item-88"><a href="https://www.gnome.org/">The GNOME Project</a>
<ul class="sub-menu">
	<li id="menu-item-89" class="menu-item menu-item-type-post_type menu-item-89"><a href="https://www.gnome.org/about/">About Us</a></li>
	<li id="menu-item-90" class="menu-item menu-item-type-custom menu-item-90"><a href="https://www.gnome.org/get-involved/">Get Involved</a></li>
	<li id="menu-item-91" class="menu-item menu-item-type-custom menu-item-91"><a href="https://www.gnome.org/teams/">Teams</a></li>
	<li id="menu-item-92" class="menu-item menu-item-type-custom menu-item-92"><a href="https://foundation.gnome.org">The GNOME Foundation</a></li>
	<li><a href="https://www.gnome.org/support-gnome/">Support GNOME</a></li>
	<li><a href="https://www.gnome.org/contact/">Contact</a></li>
</ul>
</li>
</ul></div>
<div class="menu-footer-2-container"><ul id="menu-footer-2" class="menu"><li id="menu-item-99" class="menu-item menu-item-type-post_type current-menu-item page_item page-item-20 current_page_item menu-item-99"><a href="#">Resources</a>
<ul class="sub-menu">
	<li><a href="https://help.gnome.org">Documentation</a></li>
	<li><a href="https://wiki.gnome.org">Wiki</a></li>
	<li><a href="https://mail.gnome.org/mailman/listinfo">Mailing Lists</a></li>
	<li><a href="https://wiki.gnome.org/GnomeIrcChannels">IRC Channels</a></li>
	<li><a href="https://gitlab.gnome.org/">Bug Tracker</a></li>
	<li><a href="https://gitlab.gnome.org/">Development Code</a></li>
	<li><a href="https://wiki.gnome.org/Jhbuild">Build Tool</a></li>
</ul>
</li>
</ul></div><div class="menu-footer-4-container"><ul id="menu-footer-4" class="menu"><li id="menu-item-104" class="menu-item menu-item-type-custom menu-item-104"><a href="http://www.gnome.org/news/">News</a>
<ul class="sub-menu">
	<li><a href="https://www.gnome.org/start/stable">Latest Release</a></li>
	<li><a href="https://planet.gnome.org">Planet GNOME</a></li>
	<li><a href="https://news.gnome.org">Development News</a></li>
	<li><a href="https://twitter.com/gnome">Twitter</a></li>
</ul>
</li>
</ul></div></div>
            <div class="links grid_3 right">
                                <div><ul class="menu available_languages"><li><strong>This website is available in many languages</strong><ul class="sub-menu"><li><a href="/languages" title="Switching Language">Switch Language</a></li></ul></li></ul>
                                
          <script type="text/javascript" src="/js/language.js" />
                                
                                </div>            </div>
            
            <!-- footnotes -->
            <div id="footnotes" class="grid_9">
                Copyright © 2005‒2014 <strong class="gnome_logo">The GNOME Project</strong><br />
                <small>Optimised for standards. Hosted by <a href="http://redhat.com">Red Hat</a>.</small>
            </div>
            
            <div class="clear"></div>
        </div>
    </div>

    <script type="text/javascript">
$(document).ready(function() {
    // Get browser
    $.each($.browser, function(i) {
        $('body').addClass(i);
        return false;
    });

    // Get OS and add it as class to body tag, this then allows forcing some
    // customisation in the CSS rules.
    var match = navigator.userAgent.toLowerCase().match(/(win|mac|linux)/);
    if (match) {
        $('body').addClass(match[0]);
    }
});
</script>

    <!-- Piwik -->
    <script type="text/javascript">
      var _paq = _paq || [];
      _paq.push(['disableCookies']);
      _paq.push(["trackPageView"]);
      _paq.push(["enableLinkTracking"]);

      (function() {
        var u=(("https:" == document.location.protocol) ? "https" : "http") + "://webstats.gnome.org/";
        _paq.push(["setTrackerUrl", u+"piwik.php"]);
        _paq.push(["setSiteId", "6"]);
        var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
        g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
      })();
    </script>
    <!-- End Piwik Code -->

</xsl:template>

</xsl:stylesheet>
