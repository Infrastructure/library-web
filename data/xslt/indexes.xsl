<?xml version='1.0' encoding='UTF-8'?><!-- -*- indent-tabs-mode: nil -*- -->
<!--
Copyright (c) 2006 Goran Rakic <grakic@devbase.net>.

This file is part of libgo.

libgo is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

libgo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libgo; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:html="http://www.w3.org/1999/xhtml"
                xmlns:gcse="uri:google-did-not-provide-a-real-ns"
                extension-element-prefixes="exsl"
                version="1.0">


<xsl:import href="heading.xsl"/>
<xsl:import href="gettext.xsl"/>
<xsl:import href="common_indexes.xsl"/>

<!-- This gets set on the command line ... -->
<xsl:param name="libgo.lang" select="''"/>
<xsl:param name="libgo.debug" select="false()"/>

<xsl:variable name="lcletters">abcdefghijklmnopqrstuvwxyz</xsl:variable>
<xsl:variable name="ucletters">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>

<xsl:output method="html" encoding="UTF-8" indent="yes"
omit-xml-declaration="yes"
doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>

<xsl:namespace-alias stylesheet-prefix="html" result-prefix="#default"/>


<xsl:template match="document[@path != '']" mode="modindex">
<xsl:param name="lang"/>
<xsl:if test="concat('/', @channel, '/', @modulename, '/') != @path">
<!-- don't write document index if the document is not versioned -->
<xsl:variable name="modulename" select="@modulename"/>

<xsl:if test="$libgo.debug">
<xsl:message>Writing <xsl:value-of
	select="concat(@channel, '/', @modulename, '/index.html.', $lang)" /></xsl:message>
</xsl:if>

<exsl:document href="{@channel}/{@modulename}/index.html.{$lang}"
method="html" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"
doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
doctype-system="http://www.w3.org/TR/html4/loose.dtd">
<html lang="{$lang}">
<head>
  <title><xsl:value-of select="title" /> - GNOME Library</title>
  <xsl:call-template name="libgo.head">
    <xsl:with-param name="channel" select="@channel"/>
  </xsl:call-template>
  <script type="text/javascript" src="/js/strings.js" />
</head>
<body class="with-star">
  <xsl:call-template name="libgo.header">
    <xsl:with-param name="channel" select="@channel"/>
    <xsl:with-param name="lang" select="$lang"/>
  </xsl:call-template>
  <div id="container" class="two_columns">
    <div class="container_12">
    <div class="page_title">
      <h1 class="article title"><xsl:if test="@icon"><img class="application-icon" alt=""><xsl:attribute name="src"><xsl:value-of select="@icon"/></xsl:attribute></img></xsl:if><a href="{@path}" lang="{@lang}"><xsl:value-of select="title"/></a></h1>
    </div>
    <div class="content">
	    <xsl:if test="abstract">
	    <p>
	     <xsl:value-of select="abstract" />
	    </p>
	    </xsl:if>

	    <xsl:if test="keywords/keyword[. = 'upcoming-deprecation']">
	      <p class="upcoming-deprecation">
	        <xsl:call-template name="gettext"><xsl:with-param name="lang"
		  select="$lang"/><xsl:with-param name="msgid"
		  select="'upcoming-deprecation'"/></xsl:call-template>
	      </p>
	    </xsl:if>
	    <xsl:if test="versions">
	      <h4 class="versions">
              <xsl:call-template name="gettext"><xsl:with-param name="lang"
              select="$lang"/><xsl:with-param name="msgid"
              select="'availableversions'"/></xsl:call-template>
	      </h4>
	      <ul class="versions">
	      <xsl:for-each select="versions/version">
	        <li>
		 <xsl:choose>
		   <xsl:when test="@keyword = 'stable'"><strong>
		    <a href="{@href}/" lang="{@lang}"><xsl:apply-templates
		 	select="." mode="version-name"><xsl:with-param name="lang" select="$lang"/>
			</xsl:apply-templates></a>
		    </strong>
		   </xsl:when>
		   <xsl:otherwise>
		    <a href="{@href}/" lang="{@lang}"><xsl:apply-templates
		 	select="." mode="version-name"><xsl:with-param name="lang" select="$lang"/>
			</xsl:apply-templates></a>
		   </xsl:otherwise>
		 </xsl:choose>
		 <xsl:if test="@keyword = 'unstable'">
		    (<xsl:call-template name="gettext"><xsl:with-param name="lang"
		      select="$lang"/><xsl:with-param name="msgid"
		      select="'development-version'"/></xsl:call-template>)
		 </xsl:if>
		</li>
	      </xsl:for-each>
	      </ul>

	      <xsl:if test="@single_page_alternative = 'true'">
	      <h4><xsl:call-template name="gettext"><xsl:with-param name="lang"
		      select="$lang"/><xsl:with-param name="msgid"
		      select="'allinonepage'"/></xsl:call-template></h4>
	      <ul class="versions">
	      <xsl:for-each select="versions/version">
	        <li>
		 <xsl:choose>
		   <xsl:when test="@keyword = 'stable'"><strong>
		     <a href="{@href}/{$modulename}.html" lang="{@lang}"><xsl:apply-templates
		 	select="." mode="version-name"><xsl:with-param name="lang" select="$lang"/>
			</xsl:apply-templates></a>
		    </strong>
		   </xsl:when>
		   <xsl:otherwise>
		     <a href="{@href}/{$modulename}.html" lang="{@lang}"><xsl:apply-templates
		 	select="." mode="version-name"><xsl:with-param name="lang" select="$lang"/>
			</xsl:apply-templates></a>
		   </xsl:otherwise>
		 </xsl:choose>
		 <xsl:if test="@keyword = 'unstable'">
		    (<xsl:call-template name="gettext"><xsl:with-param name="lang"
		      select="$lang"/><xsl:with-param name="msgid"
		      select="'development-version'"/></xsl:call-template>)
		 </xsl:if>
		</li>
	      </xsl:for-each>
	      </ul>
	      </xsl:if>
	    </xsl:if>

	  </div>
	  <div class="sidebar">
	    <xsl:if test="tarballs">
            <div class="downloads subtle_box">
            <h4>
              <xsl:call-template name="gettext"><xsl:with-param name="lang"
              select="$lang"/><xsl:with-param name="msgid"
              select="'downloads'"/></xsl:call-template>
            </h4>
	    <ul>
	      <xsl:for-each select="tarballs/tarball">
                <li><a href="{text()}"><xsl:value-of select="text()"/></a></li>
              </xsl:for-each>
	    </ul>
	    <xsl:if test="keywords/keyword[. = 'gtk-doc']">
	      <p class="devhelp-note">
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
		  select="$lang"/><xsl:with-param name="msgid"
		  select="'devhelp-note'"/></xsl:call-template>
	      </p>
	    </xsl:if>
            </div>
	    </xsl:if>

            <xsl:if test="other-languages/lang">
            <div class="other-languages subtle_box">
              <script type="text/javascript" src="/js/language.js" />
              <script type="text/javascript">display_missing_translation_text()</script>
            </div>
            </xsl:if>

            <xsl:if test="@tarballname">
            <div class="tarballname subtle_box">
              <p>
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
		  select="$lang"/><xsl:with-param name="msgid"
		  select="'tarball-location'"/></xsl:call-template>
                <xsl:text> </xsl:text>
                <xsl:value-of select="@tarballname"/>
              </p>
            </div>
            </xsl:if>

	    </div>
	  </div>
	  <div id="footer_art" class="default"> </div>
          </div>
	  <xsl:call-template name="libgo.footer"/>
	</body>
      </html>
    </exsl:document>
    </xsl:if>
  </xsl:template>

  <xsl:template match="version" mode="version-name">
    <xsl:param name="lang"/>
    <xsl:choose>
      <xsl:when test=". = 'nightly'">
        <xsl:call-template name="gettext"><xsl:with-param name="lang"
          select="$lang"/><xsl:with-param name="msgid"
	  select="'nightly-version'"/></xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="section" mode="channelindex">
    <xsl:param name="lang"/>
    <xsl:param name="hidetitle" value="false()"/>

    <xsl:if test="not($hidetitle)">
    <xsl:call-template name="category-title">
      <xsl:with-param name="lang" select="$lang"/>
      <xsl:with-param name="tocid" select="@toc_id"/>
    </xsl:call-template>
    </xsl:if>

    <xsl:for-each select="section">
      <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
      <xsl:variable name="title" select="@title"/>
      <h3 class="subsection" id="{@title}">
        <xsl:apply-templates select="document('../overlay.xml')//subsection[@id = $title]"
                             mode="title">
          <xsl:with-param name="lang" select="$lang" />
        </xsl:apply-templates>
      </h3>
      <xsl:apply-templates select="document('../overlay.xml')//subsection[@id = $title]"
                           mode="intro">
        <xsl:with-param name="lang" select="$lang" />
      </xsl:apply-templates>
      <dl class="doc-index">
      <xsl:for-each select="document">
        <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
        <xsl:sort select="translate(title, $ucletters, $lcletters)"/>
        <xsl:apply-templates select="." mode="channelindex">
          <xsl:with-param name="lang" select="$lang"/>
          <xsl:with-param name="ignoredeprecated" select="true()"/>
	</xsl:apply-templates>
      </xsl:for-each>
      </dl>
    </xsl:for-each>

    <xsl:if test="section and document">
      <h3 class="subsection">
        <xsl:call-template name="gettext">
          <xsl:with-param name="lang" select="$lang"/>
          <xsl:with-param name="msgid" select="'others'"/>
        </xsl:call-template>
      </h3>
    </xsl:if>

    <dl class="doc-index">
      <xsl:for-each select="document">
        <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
        <xsl:sort select="translate(title, $ucletters, $lcletters)"/>
        <xsl:apply-templates select="." mode="channelindex">
          <xsl:with-param name="lang" select="$lang"/>
	</xsl:apply-templates>
      </xsl:for-each>
    </dl>
  </xsl:template>

  <xsl:template match="index" mode="channelindex">
    <div class="subindex" id="subindex-{@id}">
      <h2><a href="{@id}"><xsl:value-of select="title"/></a></h2>
      <xsl:if test="abstract">
        <p><xsl:value-of select="abstract"/></p>
      </xsl:if>
    </div>
  </xsl:template>

  <xsl:template match="index" mode="toc">
  <xsl:param name="lang" value="@lang"/>

  <ul class="indextoc">
   <xsl:for-each select="section">
     <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
     <li>
      <a href="#{@toc_id}"><xsl:call-template name="gettext"><xsl:with-param name="lang"
        select="$lang"/><xsl:with-param name="msgid"
        select="@toc_id"/></xsl:call-template></a>
     <xsl:if test="section">
      <ul>
       <xsl:for-each select="section">
        <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
        <li>
        <xsl:variable name="title" select="@title"/>
        <a href="#{@title}"><xsl:apply-templates select="document('../overlay.xml')//subsection[@id = $title]"
        mode="title">
        <xsl:with-param name="lang" select="$lang" />
        </xsl:apply-templates></a>
        </li>
        </xsl:for-each>
       </ul>
      </xsl:if>
      </li>
     </xsl:for-each>
     <xsl:if test="//keywords/keyword[. = 'upcoming-deprecation']">
     <li><a href="deprecated">Deprecated APIs</a></li>
     </xsl:if>
    </ul>
  </xsl:template>

  <xsl:template match="index">
    <xsl:param name="channel" select="@channel"/>
    <xsl:param name="lang" select="@lang"/>
    <xsl:param name="filename">
      <xsl:choose>
        <xsl:when test="@id"><xsl:value-of select="@id"/></xsl:when>
        <xsl:otherwise>index</xsl:otherwise>
      </xsl:choose>
    </xsl:param>

    <xsl:if test="$libgo.debug">
      <xsl:message>Writing channel: <xsl:value-of
                select="concat(@channel, '/', $filename, '.html.', @lang)" /></xsl:message>
    </xsl:if>

    <xsl:apply-templates select="index" />

    <exsl:document href="{@channel}/{$filename}.html.{@lang}"
        method="html" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"
        doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
        doctype-system="http://www.w3.org/TR/html4/loose.dtd">
      <html lang="{@lang}">
	<head>
	  <title>
            <xsl:choose>
	      <xsl:when test="@channel = 'users'">
	        GNOME Library -
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
                select="@lang"/><xsl:with-param name="msgid"
                select="'userslabel'"/></xsl:call-template>
	      </xsl:when>
	      <xsl:when test="@channel = 'devel'">
	        GNOME Library -
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
                select="@lang"/><xsl:with-param name="msgid"
                select="'developerslabel'"/></xsl:call-template>
	      </xsl:when>
	      <xsl:when test="@channel = 'admin'">
	        GNOME Library -
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
                select="@lang"/><xsl:with-param name="msgid"
                select="'sysadminslabel'"/></xsl:call-template>
	      </xsl:when>
	      <xsl:otherwise>
	        GNOME Library
	      </xsl:otherwise>
	    </xsl:choose>
	  </title>
	  <xsl:call-template name="libgo.head">
	    <xsl:with-param name="channel" select="@channel"/>
	  </xsl:call-template>
          <script type="text/javascript" src="/js/strings.js" />
	  <xsl:if test="$filename = 'references'">
            <xsl:if test="$libgo.dbm_support">
              <link rel="stylesheet" type="text/css" href="/skin/jquery.autocomplete.css"/>
              <script type="text/javascript" src="/js/jquery.js" />
              <script type="text/javascript" src="/js/jquery.autocomplete.js" />
            </xsl:if>
          </xsl:if>
	</head>
	<body class="with-star">
	  <xsl:call-template name="libgo.header">
            <xsl:with-param name="channel" select="@channel"/>
            <xsl:with-param name="lang" select="@lang"/>
          </xsl:call-template>
	  <div id="container" class="two_columns">
	    <div class="container_12">
	    <xsl:if test="title">
	      <div class="page_title"><h1 class="subindex" id="subindex-{@id}"><xsl:value-of select="title"/></h1></div>
	    </xsl:if>
            <div class="content">
	    <xsl:apply-templates select="section" mode="channelindex">
              <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
	      <xsl:sort select="translate(@toc_id, $ucletters, $lcletters)" />
              <xsl:with-param name="lang" select="@lang"/>
	      <xsl:with-param name="hidetitle" select="count(section) = 1"/>
	    </xsl:apply-templates>
	    <xsl:apply-templates select="index" mode="channelindex">
              <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
	    </xsl:apply-templates>
            </div>
	  </div>
          <div>
            <xsl:attribute name="class">
              sidebar
              <xsl:if test="not(title)"> notitle</xsl:if>
            </xsl:attribute>
            <xsl:if test="@channel = 'users'">
              <div id="usr" class="subtle_box">
                <h2><span>
                  <xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="@lang"/><xsl:with-param name="msgid"
                    select="'userslabel'"/></xsl:call-template>
                </span></h2>
                <p>
                  <xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="@lang"/><xsl:with-param name="msgid"
                    select="'userstext'"/></xsl:call-template>
                </p>
              </div>
            </xsl:if>
	    <xsl:if test="$filename = 'references'">
	      <div class="subtle_box">
              <xsl:apply-templates select="." mode="toc">
                <xsl:with-param name="lang" select="@lang"/>
	      </xsl:apply-templates>
	      </div>
	    </xsl:if>

            <xsl:if test="@channel = 'admin'">
              <div id="adm" class="subtle_box">
                <h2><span>
                  <xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="@lang"/><xsl:with-param name="msgid"
                    select="'sysadminslabel'"/></xsl:call-template>
                </span></h2>
                <p>
                  <xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="@lang"/><xsl:with-param name="msgid"
                    select="'sysadminstext'"/></xsl:call-template>
                </p>
              </div>
            </xsl:if>

          </div>
	  <div id="footer_art" class="default"> </div>
	  </div>
	  <xsl:call-template name="libgo.footer"/>
	</body>
      </html>
    </exsl:document>
  </xsl:template>

  <xsl:template match="home">
    <xsl:if test="$libgo.debug">
      <xsl:message>Writing home: <xsl:value-of select="concat('index.html.', @lang)" /></xsl:message>
    </xsl:if>
    <exsl:document href="index.html.{@lang}"
        method="html" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"
        doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
        doctype-system="http://www.w3.org/TR/html4/loose.dtd">
      <html lang="{@lang}">
	<head>
	  <title>GNOME Library</title>
	  <xsl:call-template name="libgo.head"/>
	  <xsl:comment>[if IE]&gt;
&lt;style&gt;
div.body-sidebar { width: 100%; }
&lt;/style&gt;
&lt;![endif]</xsl:comment><xsl:text>
</xsl:text>
          <script type="text/javascript" src="/js/strings.js" />
	</head>
	<body class="with-star">
	  <xsl:call-template name="libgo.header">
            <xsl:with-param name="channel" select="'home'"/>
            <xsl:with-param name="lang" select="@lang"/>
          </xsl:call-template>
	  <div id="container" class="two_columns">
            <div class="content">
            <div id="usr" class="homeblock">
              <h2><a href="users/">
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
                select="@lang"/><xsl:with-param name="msgid"
                select="'userslabel'"/></xsl:call-template>
              </a></h2>
              <p>
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
                select="@lang"/><xsl:with-param name="msgid"
                select="'userstext'"/></xsl:call-template>
              </p>
            </div>

            <div id="adm" class="homeblock">
              <h2><a href="admin/">
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
                select="@lang"/><xsl:with-param name="msgid"
                select="'sysadminslabel'"/></xsl:call-template>
              </a></h2>
              <p>
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
                select="@lang"/><xsl:with-param name="msgid"
                select="'sysadminstext'"/></xsl:call-template>
              </p>
            </div>

            <div id="dev" class="homeblock">
              <h2><a href="https://developer.gnome.org/">
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
                select="@lang"/><xsl:with-param name="msgid"
                select="'developerslabel'"/></xsl:call-template>
              </a></h2>
              <p>
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
                select="@lang"/><xsl:with-param name="msgid"
                select="'developerstext'"/></xsl:call-template>
              </p>
            </div>

	  </div>
	  <div id="footer_art" class="default"> </div>
          </div>
	  <xsl:call-template name="libgo.footer"/>
	</body>
      </html>
    </exsl:document>
  </xsl:template>

  <!-- /nightly, listing all nightly generated documents (bug 556426) -->
  <xsl:template match="document" mode="nightly">
  </xsl:template>

  <xsl:template match="indexes" mode="nightly">
    <exsl:document href="nightly.html"
        method="html" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"
        doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
        doctype-system="http://www.w3.org/TR/html4/loose.dtd">
      <html>
	<head>
	  <title>Nightly Documents - GNOME Library</title>
	  <xsl:call-template name="libgo.head"/>
          <script type="text/javascript" src="/js/strings.js" />
	</head>
	<body class="with-star">
	  <xsl:call-template name="libgo.header">
          </xsl:call-template>
	  <div id="container" class="two_columns">
	    <div class="container_12">
	  <div class="page_title"><h1 class="title">Nightly Generated Documents</h1></div>
	  <ul>
	  <xsl:for-each select="index[@lang = 'en']//document">
	  <xsl:if test="versions/version[@href='nightly'] = 'nightly'">
	  <li><a href="{@path}../nightly/"><xsl:value-of select="title"/></a></li>
	  </xsl:if>
	  </xsl:for-each>
	  </ul>
	  </div>
	  <div id="footer_art" class="default"> </div>
	  </div>
	  <xsl:call-template name="libgo.footer"/>
	</body>
      </html>
    </exsl:document>

  </xsl:template>

  <xsl:template match="home" mode="languages">
    <xsl:message>Writing languages.html.<xsl:value-of select="@lang"/></xsl:message>
    <exsl:document href="languages.html.{@lang}"
        method="html" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"
        doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
        doctype-system="http://www.w3.org/TR/html4/loose.dtd">
      <html lang="{@lang}">
        <head>
          <title>
          <xsl:call-template name="gettext">
            <xsl:with-param name="lang" select="@lang"/>
            <xsl:with-param name="msgid" select="'switchlang'"/>
            </xsl:call-template>
          —
          <xsl:call-template name="gettext">
            <xsl:with-param name="lang" select="@lang"/>
            <xsl:with-param name="msgid" select="'sitetitle'"/>
          </xsl:call-template>
          </title>
          <xsl:call-template name="libgo.head"/>
          <script type="text/javascript" src="/js/strings.js" />
        </head>
        <body class="with-star">
          <xsl:call-template name="libgo.header"/>
          <div id="container" class="two_columns">
            <div class="page_title"><h1 class="title">
              <xsl:call-template name="gettext">
                <xsl:with-param name="lang" select="@lang"/>
                <xsl:with-param name="msgid" select="'switchlang'"/>
              </xsl:call-template>
            </h1></div>
            <div class="content">

            <p>
              <xsl:call-template name="gettext">
                <xsl:with-param name="lang" select="@lang"/>
                <xsl:with-param name="msgid" select="'activelang'"/>
              </xsl:call-template>
              <xsl:text> </xsl:text>
                <xsl:call-template name="language-label">
                  <xsl:with-param name="lang" select="@lang"/>
                </xsl:call-template>
            </p>

            <ul class="language-list">
              <xsl:for-each select="//home">
                <xsl:sort select="@lang"/>
                <li><a href="index.html.{@lang}"><xsl:call-template name="language-label">
                  <xsl:with-param name="lang" select="@lang"/>
                </xsl:call-template></a>
                <xsl:text> </xsl:text>
                <span class="lang-code">(<xsl:value-of select="@lang"/>)</span>
                </li>
              </xsl:for-each>
            </ul>
            </div>
            <div class="sidebar">
              <script type="text/javascript" src="/js/language.js" />
              <script type="text/javascript">display_remove_cookie_text()</script>
            </div>
            <div id="footer_art" class="default"> </div>
          </div>
          <xsl:call-template name="libgo.footer"/>
        </body>
      </html>
    </exsl:document>
  </xsl:template>

  <xsl:template match="home" mode="search">
    <xsl:message>Writing search.html.<xsl:value-of select="@lang"/></xsl:message>
    <exsl:document href="search.html.{@lang}"
        method="html" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"
        doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
        doctype-system="http://www.w3.org/TR/html4/loose.dtd">
      <html lang="{@lang}">
        <head>
          <title>
          <xsl:call-template name="gettext">
            <xsl:with-param name="lang" select="@lang"/>
            <xsl:with-param name="msgid" select="'search'"/>
            </xsl:call-template>
          —
          <xsl:call-template name="gettext">
            <xsl:with-param name="lang" select="@lang"/>
            <xsl:with-param name="msgid" select="'sitetitle'"/>
          </xsl:call-template>
          </title>
          <xsl:call-template name="libgo.head"/>
          <script type="text/javascript" src="/js/strings.js" />
        </head>
        <body class="with-star">
          <xsl:call-template name="libgo.header"/>
          <div id="container" class="two_columns">
            <div class="page_title"><h1 class="title">
              <xsl:call-template name="gettext">
                <xsl:with-param name="lang" select="@lang"/>
                <xsl:with-param name="msgid" select="'search'"/>
              </xsl:call-template>
            </h1></div>
            <div class="content">

<script>
(function() {
 var cx = '003454451080581476612:osbhtsmrn9g'; // Insert your own Custom Search engine ID here
 var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true;
 gcse.src = (document.location.protocol == 'https' ? 'https:' : 'https:') + '//www.google.com/cse/cse.js?cx=' + cx;
 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s);
})();
</script>
<!-- and that namespace is not defined anywhere, luckily xsltproc do not barf on this... -->
<gcse:search></gcse:search>

            </div>
            <div class="sidebar">
            </div>
            <div id="footer_art" class="default"> </div>
          </div>
          <xsl:call-template name="libgo.footer"/>
        </body>
      </html>
    </exsl:document>
  </xsl:template>

  <!-- JavaScript related translations -->
  <xsl:template match="home" mode="javascript">
    <xsl:param name="lang" select="@lang"/>
    <xsl:param name="language_cookie"
      select="document('../catalog.xml')/msgcat/msgstr[@msgid =
        'js-language-cookie' and ($lang = 'en' or @xml:lang = $lang)]"/>
    <xsl:param name="language_missing"
      select="document('../catalog.xml')/msgcat/msgstr[@msgid =
        'js-language-missing' and ($lang = 'en' or @xml:lang = $lang)]"/>
    <xsl:param name="remove_cookie"
      select="document('../catalog.xml')/msgcat/msgstr[@msgid =
        'js-remove-cookie' and ($lang = 'en' or @xml:lang = $lang)]"/>
    <xsl:if test="$lang = 'en' or ($language_cookie and $language_missing and $remove_cookie)">
      <exsl:document href="js/strings.js.{$lang}" method="text">
var language_cookie_text = "<xsl:value-of select="$language_cookie"/>";
var language_missing_text = "<xsl:value-of select="$language_missing"/>";
var remove_cookie_text = "<xsl:value-of select="$remove_cookie"/>";
      </exsl:document>
    </xsl:if>
  </xsl:template>

  <xsl:template match="indexes">
    <xsl:apply-templates select="node()"/>
    <xsl:for-each select="home">
      <xsl:variable name="lang" select="@lang"/>
      <xsl:apply-templates select="../index[@lang = $lang]//document" mode="modindex">
        <xsl:with-param name="lang" select="$lang"/>
      </xsl:apply-templates>
    </xsl:for-each>
    <xsl:apply-templates select="." mode="nightly"/>
    <xsl:apply-templates select="." mode="languages"/>
    <xsl:apply-templates select="." mode="search"/>
    <xsl:apply-templates select="home" mode="javascript"/>
  </xsl:template>

</xsl:stylesheet>
