<?xml version='1.0' encoding='UTF-8'?>
<!--
Copyright (c) 2006 Goran Rakic <grakic@devbase.net>.

This file is part of libgo.

libgo is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

libgo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libgo; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:html="http://www.w3.org/1999/xhtml"
                xmlns:gcse="uri:google-did-not-provide-a-real-ns"
                extension-element-prefixes="exsl"
                version="1.0">

<xsl:import href="gettext.xsl"/>

<xsl:template name="footer">
<div id="footer">
<p>
Copyright © 2007-2014 <a href="https://www.gnome.org/">The GNOME Project</a>.
<br />
GNOME and the foot logo are trademarks of the GNOME Foundation.
<br />
Hosted documents have their own copyright notices.
<br />
<a href="http://validator.w3.org/check/referer">Optimised</a> for <a
href="http://www.w3.org/">standards</a>.
Hosted by <a href="http://www.redhat.com/">Red Hat</a>.
</p>
</div>
</xsl:template>

<xsl:template name="category-title">
<xsl:param name="lang"/>
<xsl:param name="tocid"/>

<xsl:variable name="rtocid">
<xsl:choose>
<xsl:when test="document('../externals/toc.xml')//toc[@id = $tocid]">
  <xsl:value-of select="$tocid"/>
</xsl:when>
<!-- Rarian 0.8 changed all toc id; as library still needs to handle
     older documents, here is a quick mapping -->
<xsl:when test="$tocid = 'index'">Core</xsl:when>
<xsl:when test="$tocid = 'ApplicationsAccessibility'">Accessibility</xsl:when>
<xsl:when test="$tocid = 'ApplicationsAccessories'">Utility</xsl:when>
<xsl:when test="$tocid = 'ApplicationsEducation'">Education</xsl:when>
<xsl:when test="$tocid = 'ApplicationsGames'">Game</xsl:when>
<xsl:when test="$tocid = 'ApplicationsGraphics'">Graphics</xsl:when>
<xsl:when test="$tocid = 'ApplicationsInternet'">Network</xsl:when>
<xsl:when test="$tocid = 'ApplicationsOffice'">Office</xsl:when>
<xsl:when test="$tocid = 'ApplicationsOther'">Other</xsl:when>
<xsl:when test="$tocid = 'DesktopApplets'">Other</xsl:when>
<xsl:when test="$tocid = 'ApplicationsScientific'">Science</xsl:when>
<xsl:when test="$tocid = 'ApplicationsMultimedia'">AudioVideo</xsl:when>
<xsl:when test="$tocid = 'ApplicationsSystem'">System</xsl:when>
</xsl:choose>
</xsl:variable>

<xsl:choose>
<xsl:when test="document('../externals/toc.xml')//toc[@id = $rtocid]/title">
<xsl:variable name="cat-icon"
	select="document('../externals/toc.xml')//toc[@id = $rtocid]/@icon"/>
<h2>
  <xsl:attribute name="class">category<xsl:if test="$cat-icon != ''"
	> cat-<xsl:value-of select="$cat-icon"/></xsl:if></xsl:attribute>
  <xsl:if test="$cat-icon != ''">
    <xsl:attribute name="id"><xsl:value-of select="$cat-icon"/></xsl:attribute>
  </xsl:if>
<xsl:choose>
  <xsl:when test="document('../externals/toc.xml')//toc[@id = $rtocid]/title[@xml:lang = $lang]">
    <xsl:value-of select="document('../externals/toc.xml')//toc[@id = $rtocid]/title[@xml:lang = $lang]"/>
  </xsl:when>
  <xsl:otherwise>
    <xsl:value-of select="document('../externals/toc.xml')//toc[@id = $rtocid]/title"/>
  </xsl:otherwise>
</xsl:choose>
</h2>
</xsl:when>
<xsl:otherwise>
<h2 class="category cat-{$tocid}" id="{$tocid}">
  <xsl:call-template name="gettext"><xsl:with-param name="lang"
    select="$lang"/><xsl:with-param name="msgid"
    select="$tocid"/></xsl:call-template>
</h2>
</xsl:otherwise>
</xsl:choose>

</xsl:template>

<xsl:template name="language-label">
<xsl:param name="lang"/>
<xsl:choose>
<xsl:when test="document('../languages.xml')//lang[@code = $lang]">
<xsl:value-of select="document('../languages.xml')//lang[@code =
$lang]"/>
</xsl:when>
<xsl:when test="document('../languages.xml')//lang[substring(@code, 1, 2) = $lang]">
<xsl:value-of
select="document('../languages.xml')//lang[substring(@code, 1, 2) =
$lang]"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="$lang"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="subsection" mode="title">
<xsl:param name="lang"/>
<xsl:choose>
<xsl:when test="title[@xml:lang = $lang]">
<xsl:value-of select="title[@xml:lang = $lang]"/>
</xsl:when>
<xsl:otherwise><xsl:value-of select="title"/></xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="subsection" mode="intro">
<xsl:param name="lang"/>
<xsl:if test="intro">
<p>
<xsl:choose>
<xsl:when test="intro[@xml:lang = $lang]">
  <xsl:value-of select="intro[@xml:lang = $lang]"/>
</xsl:when>
<xsl:otherwise><xsl:value-of select="intro"/></xsl:otherwise>
</xsl:choose>
</p>
</xsl:if>
</xsl:template>

<xsl:template name="overlay-section-title">
<xsl:param name="lang"/>
<xsl:choose>
<xsl:when test="document('../overlay.xml')//subsection[@code = $lang]">
<xsl:value-of select="document('../languages.xml')//lang[@code =
$lang]"/>
</xsl:when>
<xsl:when test="document('../languages.xml')//lang[substring(@code, 1, 2) = $lang]">
<xsl:value-of
select="document('../languages.xml')//lang[substring(@code, 1, 2) =
$lang]"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="$lang"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="document" mode="channelindex">
<xsl:param name="ignoredeprecated" select="true()"/>
<xsl:param name="lang"/>
<xsl:if test="not($ignoredeprecated) or not(keywords/keyword[. = 'upcoming-deprecation'])">
<dt>
<xsl:if test="keywords/keyword[. = 'upcoming-deprecation']">
<xsl:attribute name="class">upcoming-deprecation</xsl:attribute>
</xsl:if>
<xsl:if test="@icon"><img class="application-icon" alt=""><xsl:attribute name="src"><xsl:value-of select="@icon"/></xsl:attribute></img></xsl:if>
<a class="doc-link" lang="{@lang}">
<xsl:attribute name="href">
  <xsl:if test="@path"><xsl:value-of select="@path"/></xsl:if>
  <xsl:if test="@href"><xsl:value-of select="@href"/></xsl:if>
</xsl:attribute>
<xsl:if test="@href">
  <xsl:attribute name="class">external</xsl:attribute>
</xsl:if>
<xsl:choose>
<xsl:when test="normalize-space(title)">
  <xsl:value-of select="title" />
</xsl:when>
<xsl:otherwise>
  <xsl:value-of select="@module" />
</xsl:otherwise>
</xsl:choose>
</a>
<xsl:if test="@href">
<xsl:text> </xsl:text>
<span class="module-more">[<xsl:call-template
    name="gettext"><xsl:with-param name="lang"
    select="$lang"/><xsl:with-param name="msgid"
    select="'external-resource'"/></xsl:call-template>]</span>
</xsl:if>
</dt>

<dd>
<xsl:if test="abstract">
<p><xsl:value-of select="abstract" /></p>
</xsl:if>
<xsl:if test="keywords/keyword[. = 'upcoming-deprecation']">
<p class="upcoming-deprecation">
 <xsl:call-template name="gettext"><xsl:with-param name="lang"
      select="$lang"/><xsl:with-param name="msgid"
      select="'upcoming-deprecation'"/></xsl:call-template>
</p>
</xsl:if>

<xsl:if test="@path and versions/version">
<xsl:text> </xsl:text>
<p class="module-more"><a href="{@modulename}/"><xsl:call-template
    name="gettext"><xsl:with-param name="lang"
    select="@lang"/><xsl:with-param name="msgid"
    select="'more-versions-languages-or-options'"/></xsl:call-template></a></p>
</xsl:if>

<xsl:if test="@channel != 'devel'">
  <!-- in most cases the developer documentation doesn't have proper
       translation infrastructure, therefore we do not display the
       missing language warning as it would appear on most documents
       and clutter the page way too much -->

<xsl:if test="substring($lang, 1, 2) != substring(@lang, 1, 2) and @path">
<p class="no-translation">
  <xsl:call-template name="gettext"><xsl:with-param name="lang"
    select="$lang"/><xsl:with-param name="msgid"
    select="'missing-translation'"/></xsl:call-template>
  <xsl:if test="other-languages/lang and @modulename">
    (<a href="/{@channel}/{@modulename}/">
      <xsl:call-template name="gettext"><xsl:with-param name="lang"
	select="$lang"/><xsl:with-param name="msgid"
	select="'doc-translations'"/></xsl:call-template>
    </a>)
  </xsl:if>
</p>
</xsl:if>
</xsl:if> <!-- @channel != 'devel' -->
</dd>
</xsl:if>
</xsl:template>

</xsl:stylesheet>
