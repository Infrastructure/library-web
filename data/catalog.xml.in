<?xml version="1.0"?>
<msgcat>
  <_msgstr msgid="sitetitle">GNOME Documentation Library</_msgstr>
  <_msgstr msgid="homelabel">Home</_msgstr>
  <_msgstr msgid="userslabel">Users</_msgstr>
  <_msgstr msgid="aboutlabel">About</_msgstr>
  <_msgstr msgid="userstext">
Even though it's extremely user-friendly, GNOME is a large and complex system,
and thus, requires some learning to use to the fullest. To make that
easier, we've provided some very useful documentation.
  </_msgstr>
  <_msgstr msgid="developerslabel">Developers</_msgstr>
  <_msgstr msgid="developerstext">
For those who develop, or are interested in developing GNOME and applications
for GNOME. You will find developer documentation and information on how to get
involved, and much more, in the GNOME Developer Center.
  </_msgstr>
  <_msgstr msgid="sysadminslabel">Administrators</_msgstr>
  <_msgstr msgid="sysadminstext">
Across the world, there are many large to small deployments of GNOME, with
their specific needs, and system administrators to manage them.  Here you will
find information on tools and methods to work with many GNOME desktops.
  </_msgstr>
  <_msgstr msgid="langinfo">Available Languages:</_msgstr>
  <_msgstr msgid="availableversions">Available Versions:</_msgstr>
  <_msgstr msgid="api-base">Core Libraries</_msgstr>
  <_msgstr msgid="api">Other Libraries</_msgstr>
  <_msgstr msgid="whitepapers">White Papers</_msgstr>
  <_msgstr msgid="guides">Guides</_msgstr>
  <_msgstr msgid="devel-guides">Development Guides</_msgstr>
  <_msgstr msgid="how-do-i">How Do I...?</_msgstr>
  <_msgstr msgid="c++-development">C++ Development</_msgstr>
  <_msgstr msgid="manuals">Manuals</_msgstr>
  <_msgstr msgid="tutorials">Tutorials</_msgstr>
  <_msgstr msgid="faqs">Frequently Asked Questions</_msgstr>
  <_msgstr msgid="others">Others</_msgstr>
  <_msgstr msgid="overview">Overview</_msgstr>
  <_msgstr msgid="programming-tools">Tools</_msgstr>
  <_msgstr msgid="gdp-documentation">GNOME Documentation Project</_msgstr>
  <_msgstr msgid="missing-translation">
    There is no translation of this documentation for your language;
    the documentation in its original language is displayed instead.
  </_msgstr>
  <_msgstr msgid="doc-translations">see other translations for this documentation</_msgstr>
  <_msgstr msgid="development-version">development version</_msgstr>
  <_msgstr msgid="searchlabel">Search</_msgstr>
  <_msgstr msgid="lookuplabel">Lookup Symbol</_msgstr>
  <_msgstr msgid="development-version-doc">documentation on development version</_msgstr>
  <_msgstr msgid="see-also">See also:</_msgstr>
  <_msgstr msgid="upcoming-deprecation">
    This module is heading towards planned deprecation. It will continue to be
    supported and API/ABI stable throughout the GNOME 2.x series, but we do not
    recommend using it in new applications unless you require functionality
    that has not already been moved elsewhere.
  </_msgstr>
  <_msgstr msgid="standards">Standards</_msgstr>
  <_msgstr msgid="api-base-libs">Core Libraries</_msgstr>
  <_msgstr msgid="api-bindings">Language Bindings</_msgstr>
  <_msgstr msgid="api-desktop-plugins">Plugins for GNOME Application</_msgstr>
  <_msgstr msgid="js-language-cookie">
    Preferred language is loaded from a cookie.
  </_msgstr>
  <_msgstr msgid="js-language-missing">
    Unable to display document in preferred language loaded from cookie,
    as translation probably does not exist.
  </_msgstr>
  <_msgstr msgid="js-remove-cookie">Remove cookie</_msgstr>
  <_msgstr msgid="downloads">Downloads</_msgstr>
  <_msgstr msgid="devhelp-note">
    Note the API references are usually available as packages in the
    distributions and visible via the Devhelp tool.
  </_msgstr>
  <_msgstr msgid="more-versions-languages-or-options">
    Previous Versions
  </_msgstr>
  <_msgstr msgid="external-resource">external resource</_msgstr>
  <_msgstr msgid="siteaction-home">Home</_msgstr>
  <_msgstr msgid="siteaction-news">News</_msgstr>
  <_msgstr msgid="siteaction-projects">Projects</_msgstr>
  <_msgstr msgid="siteaction-art">Art</_msgstr>
  <_msgstr msgid="siteaction-support">Support</_msgstr>
  <_msgstr msgid="siteaction-development">Development</_msgstr>
  <_msgstr msgid="siteaction-community">Community</_msgstr>
  <_msgstr msgid="allinonepage">
    Same documents, formatted as a single HTML file
  </_msgstr>
  <_msgstr msgid="nightly-version">Nightly</_msgstr>
  <_msgstr msgid="devel-resource">External Tools and Resources</_msgstr>
  <_msgstr msgid="ApplicationsProgramming">Manuals</_msgstr>
  <_msgstr msgid="switchlang">Switching Language</_msgstr>
  <_msgstr msgid="activelang">Active Language:</_msgstr>
  <_msgstr msgid="gnome-developer-center">GNOME Developer Center</_msgstr>
  <_msgstr msgid="10-minute-tutorials">Code examples and demos</_msgstr>
  <_msgstr msgid="getting-started">Getting Started</_msgstr>
  <_msgstr msgid="platform-overview">Application development overview</_msgstr>
  <_msgstr msgid="platform-overview-intro">Guide to the GNOME platform and libraries for developers.</_msgstr>
  <_msgstr msgid="quick-api-lookup">Quick Lookup</_msgstr>
  <_msgstr msgid="user-interface">User Interface</_msgstr>
  <_msgstr msgid="multimedia">Multimedia</_msgstr>
  <_msgstr msgid="communication">Communication</_msgstr>
  <_msgstr msgid="data-storage">Data Storage</_msgstr>
  <_msgstr msgid="utilities">Utilities</_msgstr>
  <_msgstr msgid="core">Core</_msgstr>
  <_msgstr msgid="system-integration">System Integration</_msgstr>
  <_msgstr msgid="desktop-integration">Desktop Integration</_msgstr>
  <_msgstr msgid="nightly-documents">Nightly Generated Documents</_msgstr>
  <_msgstr msgid="deprecated-api-references">Deprecated API References</_msgstr>
  <_msgstr msgid="welcome-to-gnome-devcenter">Welcome to GNOME. Getting started is easy - install our dev tools and take a look at a tutorial.</_msgstr>
  <_msgstr msgid="tarball-location">This documentation is generated from the following tarball:</_msgstr>
  <_msgstr msgid="search">Search</_msgstr>
  <_msgstr msgid="doc-feedback-intro">Got a comment? Spotted an error? Found the instructions unclear?</_msgstr>
  <_msgstr msgid="doc-feedback-link">Send feedback about this page.</_msgstr>
  <_msgstr msgid="gnome-platform-demos">GNOME Platform Demos</_msgstr>
  <_msgstr msgid="hig">Human Interface Guidelines</_msgstr>
  <_msgstr msgid="hig-intro">Advice and guidelines on designing effective interfaces with GTK.</_msgstr>
  <_msgstr msgid="gnome-developer-center-welcome">Welcome to the GNOME developer center! Here you will find all the information that you need to create fantastic software using GNOME technologies.</_msgstr>
</msgcat>
